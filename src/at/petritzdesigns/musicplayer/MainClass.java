package at.petritzdesigns.musicplayer;

import at.petritzdesigns.musicplayer.controller.MainController;

/**
 * MusicPlayer
 * 
 * @author Markus Petritz
 * @version 1.0.0
 */
public class MainClass {

    public static void main(String[] args) {
        MainController controller = new MainController();
        controller.showGui();
    }

}
