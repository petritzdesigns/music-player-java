package at.petritzdesigns.musicplayer.assets.helper;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 * MusicPlayer
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class AssetsHelper {

    public static String getAssetsPath(String filename) {
        return "src/at/petritzdesigns/musicplayer/assets/" + filename;
    }
    
    public static boolean isValidMp3File(File file) {
        if(file.isDirectory()) {
            return false;
        }
        if(file.getAbsolutePath().isEmpty()) {
            return false;
        }
        if(!file.exists()) {
            return false;
        }
        if(!file.isFile()) {
            return false;
        }
        return file.getAbsolutePath().endsWith(".mp3");
    }

    public static BufferedImage getAsset(String filename) {
        BufferedImage iconStop = null;

        try {
            iconStop = ImageIO.read(new File(filename));
        } catch (IOException ex) {
            JOptionPane.showMessageDialog(null, "Unable to retrieve assets pack.");
            System.err.println(ex);
        }

        return iconStop;
    }

    public static BufferedImage scaleAsset(BufferedImage img, int width, int heigth) {
        return AssetsHelper.toBufferedImage(img.getScaledInstance(width, heigth, Image.SCALE_SMOOTH));
    }

    public static BufferedImage toBufferedImage(Image img) {
        if (img instanceof BufferedImage) {
            return (BufferedImage) img;
        }

        BufferedImage bimage = new BufferedImage(img.getWidth(null), img.getHeight(null), BufferedImage.TYPE_INT_ARGB);

        Graphics2D bGr = bimage.createGraphics();
        bGr.drawImage(img, 0, 0, null);
        bGr.dispose();

        return bimage;
    }

    public static BufferedImage toBufferedImage(Icon ico) {
        if (!(ico instanceof ImageIcon)) {
            BufferedImage bi = new BufferedImage(
                    ico.getIconWidth(),
                    ico.getIconHeight(),
                    BufferedImage.TYPE_INT_RGB);
            Graphics2D g = bi.createGraphics();
            ico.paintIcon(null, g, 0, 0);
            g.dispose();
            return bi;
        }
        ImageIcon icon = (ImageIcon) ico;
        return (BufferedImage) icon.getImage();
    }

    public static BufferedImage coloredImage(BufferedImage image, Color color) {
        int width = image.getWidth();
        int height = image.getHeight();
        WritableRaster raster = image.getRaster();

        for (int xx = 0; xx < width; xx++) {
            for (int yy = 0; yy < height; yy++) {
                int[] pixels = raster.getPixel(xx, yy, (int[]) null);
                pixels[0] = color.getRed();
                pixels[1] = color.getGreen();
                pixels[2] = color.getBlue();
                raster.setPixel(xx, yy, pixels);
            }
        }
        return image;
    }

}
