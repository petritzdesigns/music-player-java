package at.petritzdesigns.musicplayer.controller;

import at.petritzdesigns.musicplayer.enums.LogType;
import at.petritzdesigns.musicplayer.helper.Logger;
import at.petritzdesigns.musicplayer.view.MainViewController;

/**
 * MusicPlayer
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class MainController {

    private MainViewController gui;

    public MainController() {

    }

    public void showGui() {
        try {
            java.awt.EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                    try {
                        gui = new MainViewController();
                        gui.setVisible(true);
                    } catch (Exception ex) {
                        Logger.getDefault().log(LogType.LOG_ERROR, ex);
                    }
                }
            });
        } catch (Exception ex) {
            Logger.getDefault().log(LogType.LOG_ERROR, ex);
        }
    }

}
