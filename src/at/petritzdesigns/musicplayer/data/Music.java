package at.petritzdesigns.musicplayer.data;

import at.petritzdesigns.musicplayer.model.Mp3Metadata;
import java.io.File;

/**
 * MusicPlayer
 * 
 * @author Markus Petritz
 * @version 1.0.0
 */
public class Music {
    
    private String album;
    private String artist;
    private Time duration;
    private String flags;
    private String song;
    
    private final File file;

    public Music(String song, String artist, String album, Time duration, File file) {
        this.album = album;
        this.artist = artist;
        this.duration = duration;
        this.flags = "  ";
        this.song = song;
        this.file = file;
    }

    public Music(Mp3Metadata metadata, File file) throws Exception {
        this.album = metadata.get().getAlbum();
        this.artist = metadata.get().getArtist();
        this.duration = new Time(metadata.getMp3file().getLengthInSeconds());
        this.flags = "  ";
        this.song = metadata.get().getTitle();
        this.file = file;
    }

    public String getAlbum() {
        return album;
    }

    public void setAlbum(String album) {
        this.album = album;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public Time getDuration() {
        return duration;
    }

    public void setDuration(Time duration) {
        this.duration = duration;
    }

    public String getFlags() {
        return flags;
    }

    public void setFlags(String flags) {
        this.flags = flags;
    }

    public String getSong() {
        return song;
    }

    public void setSong(String song) {
        this.song = song;
    }

    public File getFile() {
        return file;
    }
    
    public void setPlayed() {
        StringBuilder sb = new StringBuilder(flags);
        sb.setCharAt(0, '✔');
        this.flags = sb.toString();
    }
    
    public void setStar() {
        StringBuilder sb = new StringBuilder(flags);
        sb.setCharAt(1, '★');
        this.flags = sb.toString();
    }
    
    public void removePlayed() {
        StringBuilder sb = new StringBuilder(flags);
        sb.setCharAt(0, ' ');
        this.flags = sb.toString();
    }
    
    public void removeStar() {
        StringBuilder sb = new StringBuilder(flags);
        sb.setCharAt(1, ' ');
        this.flags = sb.toString();
    }

    @Override
    public String toString() {
        return String.format("%s: %s (%s)", song, artist, duration.toString());
    }
}
