package at.petritzdesigns.musicplayer.data;

/**
 * MusicPlayer
 * 
 * @author Markus Petritz
 * @version 1.0.0
 * @see http://petritzdesigns.at
 */
public class Time {
    
    private int seconds;
    private int minutes;

    public Time() {
        this(0, 0);
    }
    
    public Time(int seconds) {
        this.minutes = seconds / 60;
        this.seconds = seconds % 60;
    }
    
    public Time(long seconds) {
        this.minutes = (int) (seconds / 60);
        this.seconds = (int) (seconds % 60);
    }
    
    public Time(int seconds, int minutes) {
        this.seconds = seconds;
        this.minutes = minutes;
    }
    
    public Time(String toparse) {
        String[] strings = toparse.split(":");
        this.minutes = Integer.parseInt(strings[0]);
        this.seconds = Integer.parseInt(strings[1]);
    }

    @Override
    public String toString() {
        return String.format("%02d:%02d", minutes, seconds);
    }

}
