package at.petritzdesigns.musicplayer.enums;

/**
 * MusicPlayer
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public enum LibraryHeader {

    SONG("Song"),
    ARTIST("Künstler"),
    ALBUM("Album"),
    DURATION("Dauer"),
    FLAGS("");

    private final String name;

    private LibraryHeader(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
