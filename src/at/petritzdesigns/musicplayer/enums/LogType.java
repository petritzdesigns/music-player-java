package at.petritzdesigns.musicplayer.enums;

/**
 * MusicPlayer
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public enum LogType {
    LOG_DEBUG,
    LOG_ERROR,
    LOG_WARNING,
    LOG_INFO,
    LOG_UNIMPORTANT,
    LOG_BADERROR,
    LOG_NORMAL
}