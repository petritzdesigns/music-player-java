package at.petritzdesigns.musicplayer.exceptions;

import at.petritzdesigns.musicplayer.enums.LogType;

/**
 * MusicPlayer
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class BaseException extends Exception {

    private final LogType type;
    
    public BaseException() {
        this.type = LogType.LOG_ERROR;
    }

    /**
     * Constructs an instance of <code>BaseException</code> with the specified
     * detail message.
     *
     * @param msg the detail message.
     */
    public BaseException(String msg) {
        super(msg);
        this.type = LogType.LOG_ERROR;
    }
    
    public BaseException(String msg, LogType type) {
        super(msg);
        this.type = type;
    }

    public LogType getType() {
        return type;
    }
}
