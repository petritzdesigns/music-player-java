package at.petritzdesigns.musicplayer.exceptions;

import at.petritzdesigns.musicplayer.enums.LogType;

/**
 * MusicPlayer
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class FatalException extends BaseException {
    
    private final int exitCode;

    public FatalException() {
        this.exitCode = FatalException.ErrorCodes.ERROR_UNKNOWN.value;
    }

    public FatalException(int exitCode, String msg) {
        super(msg, LogType.LOG_BADERROR);
        this.exitCode = exitCode;
    }

    public FatalException(int exitCode, String msg, LogType type) {
        super(msg, type);
        this.exitCode = exitCode;
    }

    public int getExitCode() {
        return exitCode;
    }
    
    public static enum ErrorCodes {
        ERROR_UNKNOWN(10000);
        
        public int value;
        private ErrorCodes(int value) {
            this.value = value;
        }
    }
}
