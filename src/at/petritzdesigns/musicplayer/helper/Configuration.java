package at.petritzdesigns.musicplayer.helper;

import at.petritzdesigns.musicplayer.enums.LogType;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * MusicPlayer
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class Configuration {

    private static Configuration instance;
    private Properties properties;

    private Configuration() {
        try {
            createConfigFileIfNotExist();
            properties = new Properties();
            properties.load(new FileInputStream("config/default.pdconfig"));
        } catch (IOException ex) {
            System.err.println("I/O Fehler: " + ex.getLocalizedMessage());
        } catch (Exception ex) {
            System.err.println("Fehler: " + ex.getLocalizedMessage());
        }
    }

    public static Configuration getDefault() {
        if (instance == null) {
            instance = new Configuration();
        }
        return instance;
    }

    public String get(String key) {
        return properties.getProperty(key);
    }

    public boolean set(String key, String value) {
        try {
            properties.setProperty(key, value);
            properties.store(new FileOutputStream("config/default.pdconfig"), "Music Player");
        }
        catch(Exception ex) {
            Logger.getDefault().log(LogType.LOG_DEBUG, ex);
            return false;
        }
        
        return true;
    }

    public boolean getBool(String key) {
        if(get(key) == null) {
            return false;
        }
        return properties.getProperty(key).equals("true");
    }

    public boolean set(String key, boolean value) {
        try {
            properties.setProperty(key, value + "");
            properties.store(new FileOutputStream("config/default.pdconfig"), "Music Player");
        }
        catch(Exception ex) {
            Logger.getDefault().log(LogType.LOG_DEBUG, ex);
            return false;
        }
        
        return true;
    }
    
    public boolean delete(String key) {
        try {
            properties.remove(key);
            properties.store(new FileOutputStream("config/default.pdconfig"), "Music Player");
        }
        catch(Exception ex) {
            Logger.getDefault().log(LogType.LOG_DEBUG, ex);
            return false;
        }
        
        return true;
    }
    
    private void createConfigFileIfNotExist() throws IOException {
        File dir = new File("config");
        File name = new File("config/default.pdconfig");
        
        if(!dir.exists()) {
            dir.mkdir();
        }
        if(!name.exists()) {
            name.createNewFile();
        }
    }
}
