package at.petritzdesigns.musicplayer.helper;

import at.petritzdesigns.musicplayer.enums.LogType;
import java.awt.Color;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;
import javax.swing.JTextPane;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;

/**
 * MusicPlayer
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class Logger {

    private static Logger instance;
    private JTextPane textPane;
    private BufferedWriter bw;

    private Logger() {
        try {
            createLogFileIfNotExists();
            bw = new BufferedWriter(new FileWriter(Configuration.getDefault().get("logFileDir") + "/" + Configuration.getDefault().get("logFileName"), true));
        } catch (IOException ex) {
            System.err.println("I/O Fehler: " + ex.getLocalizedMessage());
        } catch (Exception ex) {
            System.err.println("Fehler: " + ex.getLocalizedMessage());
        }
    }

    public static Logger getDefault() {
        if (instance == null) {
            instance = new Logger();
        }
        return instance;
    }

    public void setTextPane(JTextPane pane) {
        this.textPane = pane;
    }

    public void log(String text) {
        write(text);
    }

    public void log(Exception ex) {
        write(getExceptionAsUsefulString(ex));
    }

    public void log(LogType type, String text) {
        write(String.format("%s: %s", getTypeAsString(type), text));
    }

    public void log(LogType type, Exception ex) {
        write(String.format("%s: %s", getTypeAsString(type), getExceptionAsUsefulString(ex)));
    }

    private void createLogFileIfNotExists() throws IOException {
        File dir = new File(Configuration.getDefault().get("logFileDir"));
        File name = new File(Configuration.getDefault().get("logFileDir") + "/" + Configuration.getDefault().get("logFileName"));

        if (!dir.exists()) {
            dir.mkdir();
        }
        if (!name.exists()) {
            name.createNewFile();
        }
    }

    private void write(String text) {
        //output if debug
        if (Configuration.getDefault().getBool("isDebug")) {
            
            StringBuilder buffer = new StringBuilder();
            
            for (int i = 3; i < 6; ++i) {
                if(Thread.currentThread().getStackTrace().length < i) {
                    break;
                }
                String fullClassName = Thread.currentThread().getStackTrace()[i].getClassName();
                String className = fullClassName.substring(fullClassName.lastIndexOf(".") + 1);
                String methodName = Thread.currentThread().getStackTrace()[i].getMethodName();
                int lineNumber = Thread.currentThread().getStackTrace()[i].getLineNumber();
                buffer.append(className).append(".").append(methodName).append("():").append(lineNumber).append("\n");
            }
            buffer.append("---------------------------------------------------\t\n").append(text).append("\n");
            System.out.println(buffer.toString());
        }

        //Write to File if defined
        if (Configuration.getDefault().getBool("shouldWriteLog")) {
            try {
                bw.write(currentTime() + " ");
                bw.write(text);
                bw.newLine();
                bw.flush();
            } catch (IOException ex) {
                System.err.println("I/O Fehler: " + ex.getLocalizedMessage());
            } catch (Exception ex) {
                System.err.println("Fehler: " + ex.getLocalizedMessage());
            }
        }

        //Write to TextArea if defined
        if (textPane != null) {
            /*
             TODO:
             Different Styles for Different Types
             */
            StyledDocument doc = textPane.getStyledDocument();
            SimpleAttributeSet keyWord = new SimpleAttributeSet();
            StyleConstants.setForeground(keyWord, Color.red);
            StyleConstants.setBackground(keyWord, Color.white);
            try {
                doc.insertString(doc.getLength(), text.concat("\n"), keyWord);
            } catch (BadLocationException ex) {
                Logger.getDefault().log(LogType.LOG_ERROR, ex);
            }
        }
    }

    private String currentTime() {
        return new Date().toString();
    }

    private String getExceptionAsUsefulString(Exception ex) {
        StringBuilder sb = new StringBuilder();

        sb.append(ex.getClass().toGenericString());
        sb.append(" --> ");
        sb.append(ex.getMessage());

        return sb.toString();
    }

    private String getTypeAsString(LogType type) {
        String temp = "";
        switch (type) {
            case LOG_BADERROR:
                temp = "Baderror";
                break;
            case LOG_DEBUG:
                temp = "Debug";
                break;
            case LOG_ERROR:
                temp = "Error";
                break;
            case LOG_INFO:
                temp = "Info";
                break;
            case LOG_UNIMPORTANT:
                temp = "Unimportant";
                break;
            case LOG_WARNING:
                temp = "Warning";
                break;
            case LOG_NORMAL:
            default:
                break;
        }
        return temp;
    }
}
