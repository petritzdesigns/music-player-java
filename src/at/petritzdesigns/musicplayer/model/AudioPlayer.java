package at.petritzdesigns.musicplayer.model;

import at.petritzdesigns.musicplayer.helper.Logger;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import javax.swing.JOptionPane;
import javazoom.jl.player.Player;

/**
 * MusicPlayer
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @see http://petritzdesigns.at
 */
public class AudioPlayer {

    private Player player;
    private FileInputStream FIS;
    private BufferedInputStream BIS;
    private boolean canResume;
    private String path;
    private int total;
    private int stopped;
    private boolean valid;
    private boolean loop;

    public AudioPlayer() {
        player = null;
        FIS = null;
        valid = false;
        BIS = null;
        path = null;
        total = 0;
        stopped = 0;
        canResume = false;
        loop = false;
    }

    public void setLoop(boolean loop) {
        this.loop = loop;
    }

    public void setStopped(int stopped) {
        this.stopped = stopped;
    }

    public int getTotalBytes() {
        return total;
    }

    public int getRemainingBytes() {
        try {
            if (FIS == null) {
                return 0;
            }
            return FIS.available();
        } catch (IOException ex) {
            Logger.getDefault().log(ex);
        }
        return 0;
    }

    public boolean canResume() {
        return canResume;
    }

    public boolean isRunning() {
        return !canResume && stopped != 0;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public void pause() {
        try {
            stopped = FIS.available();
            player.close();
            FIS = null;
            BIS = null;
            player = null;
            if (valid) {
                canResume = true;
            }
        } catch (Exception e) {
            Logger.getDefault().log(e);
        }
    }

    public void resume() {
        if (!canResume) {
            return;
        }
        if (play(total - stopped)) {
            canResume = false;
        }
    }

    public void stop() {
        try {
            if (player != null) {
                player.close();
            }
            valid = false;
            total = 0;
            stopped = 0;
            canResume = false;

        } catch (Exception e) {
            Logger.getDefault().log(e);
        }
    }

    public boolean play(int pos) {
        valid = true;
        canResume = false;
        try {
            FIS = new FileInputStream(path);
            total = FIS.available();
            if (pos > -1) {
                FIS.skip(pos);
            }
            BIS = new BufferedInputStream(FIS);
            player = new Player(BIS);
            Thread t = new Thread(
                    new Runnable() {
                        @Override
                        public void run() {
                            try {
                                player.play();
                            } catch (Exception e) {
                                JOptionPane.showMessageDialog(null, "Error playing mp3 file");
                                Logger.getDefault().log(e);
                                valid = false;
                            }
                        }
                    }
            );
            t.start();
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error playing mp3 file");
            Logger.getDefault().log(e);
            valid = false;
        }
        return valid;
    }
}
