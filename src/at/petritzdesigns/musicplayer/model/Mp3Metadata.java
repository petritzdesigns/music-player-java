package at.petritzdesigns.musicplayer.model;

import com.mpatric.mp3agic.ID3v2;
import com.mpatric.mp3agic.InvalidDataException;
import com.mpatric.mp3agic.Mp3File;
import com.mpatric.mp3agic.UnsupportedTagException;
import java.io.File;
import java.io.IOException;

/**
 * MusicPlayer
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class Mp3Metadata {

    private final Mp3File mp3file;

    public Mp3Metadata(File file) throws IOException, UnsupportedTagException, InvalidDataException {
        this.mp3file = new Mp3File(file);
    }

    public Mp3Metadata(Mp3File mp3file) {
        this.mp3file = mp3file;
    }

    public ID3v2 get() throws Exception {
        if(mp3file.hasId3v2Tag()) {
            return mp3file.getId3v2Tag();
        }
        throw new Exception("Does not support ID3v2 Tag...");
    }

    public Mp3File getMp3file() {
        return mp3file;
    }
}
