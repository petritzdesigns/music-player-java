package at.petritzdesigns.musicplayer.model;

import at.petritzdesigns.musicplayer.data.Music;
import at.petritzdesigns.musicplayer.data.Time;
import at.petritzdesigns.musicplayer.enums.LibraryHeader;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 * MusicPlayer
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class MusicLibraryModel extends AbstractTableModel {

    private List<Music> library;

    public MusicLibraryModel() {
        this.library = new ArrayList<>();
    }

    public MusicLibraryModel(List<Music> library) {
        this.library = library;
    }

    public List<Music> getLibrary() {
        return library;
    }

    public boolean addMusic(Music music) {
        boolean temp = library.add(music);
        super.fireTableRowsInserted(library.indexOf(music), library.indexOf(music));
        return temp;
    }

    public boolean removeMusic(Music music) {
        boolean temp = library.remove(music);
        super.fireTableRowsDeleted(library.indexOf(music), library.indexOf(music));
        return temp;
    }

    public void removeMusic(int index) {
        library.remove(index);
        super.fireTableRowsDeleted(index, index);
    }

    public void changeMusic(int index, Music music) {
        library.set(index, music);
        super.fireTableRowsUpdated(index, index);
    }

    @Override
    public int getRowCount() {
        return library.size();
    }

    @Override
    public int getColumnCount() {
        return LibraryHeader.values().length;
    }

    @Override
    public String getColumnName(int column) {
        return LibraryHeader.values()[column].getName();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Music row = library.get(rowIndex);
        switch (LibraryHeader.values()[columnIndex]) {
            case ALBUM:
                return row.getAlbum();
            case ARTIST:
                return row.getArtist();
            case DURATION:
                return row.getDuration();
            case FLAGS:
                return row.getFlags();
            case SONG:
                return row.getSong();
            default:
                return " ";
        }
    }

}
