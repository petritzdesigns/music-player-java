package at.petritzdesigns.musicplayer.model;

import at.petritzdesigns.musicplayer.view.MainViewController;

/**
 * MusicPlayer
 *
 * @author Markus Petritz
 * @version 1.0.0
 * @see http://petritzdesigns.at
 */
public class TimerThread extends Thread {

    private final MainViewController view;
    private final AudioPlayer player;
    private long currentTimeSeconds;
    private final long endTime;

    public TimerThread(MainViewController view, AudioPlayer player, long endTime) {
        this.view = view;
        this.player = player;
        this.currentTimeSeconds = 0;
        this.endTime = endTime;
    }

    public long getCurrentTime() {
        return currentTimeSeconds;
    }

    public void setCurrentTime(long currentTimeSeconds) {
        this.currentTimeSeconds = currentTimeSeconds;
    }

    @Override
    public void run() {
        while (true) {
            try {
                long remainingSeconds = player.getRemainingBytes() / (player.getTotalBytes() / endTime);
                currentTimeSeconds = endTime - remainingSeconds;
                if (currentTimeSeconds == endTime) {
                    break;
                }
                view.updateTime();
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                System.err.println(ex);
            }
        }
    }
}
