package at.petritzdesigns.musicplayer.test;

import at.petritzdesigns.musicplayer.helper.Configuration;

/**
 * MusicPlayer
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class ConfigTester {

    public static void main(String[] args) {
        if(!Configuration.getDefault().set("test", "Test")) {
            System.out.println("Test failed... Could not set Configuration item");
            System.exit(0);
        }
        
        if(!"Test".equals(Configuration.getDefault().get("test"))) {
            System.out.println("Test failed... Could not get Configuration item");
            System.exit(0);
        }
        
        if(!Configuration.getDefault().delete("test")) {
            System.out.println("Test failed... Could not delete Configuration item");
            System.exit(0);
        }
        
        System.out.println("Test passed!");
    }

}
