package at.petritzdesigns.musicplayer.test;

import at.petritzdesigns.musicplayer.enums.LogType;
import at.petritzdesigns.musicplayer.helper.Logger;

/**
 * MusicPlayer
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class LogTester {

    public static void main(String[] args) {
        try {
            Logger.getDefault().log(new Exception("Test#1"));
            Logger.getDefault().log("Test#2");
            Logger.getDefault().log(LogType.LOG_INFO, new Exception("Test#3"));
            Logger.getDefault().log(LogType.LOG_ERROR, "Test#4");
        } catch (Exception ex) {
            System.out.println("Test failed...");
            System.err.println(ex);
        }

        System.out.println("Test passed");
    }

}
