package at.petritzdesigns.musicplayer.view;

import at.petritzdesigns.musicplayer.assets.helper.AssetsHelper;
import at.petritzdesigns.musicplayer.data.Music;
import at.petritzdesigns.musicplayer.data.Time;
import at.petritzdesigns.musicplayer.helper.Configuration;
import at.petritzdesigns.musicplayer.helper.Logger;
import at.petritzdesigns.musicplayer.model.AudioPlayer;
import at.petritzdesigns.musicplayer.model.Mp3Metadata;
import at.petritzdesigns.musicplayer.model.MusicLibraryModel;
import at.petritzdesigns.musicplayer.model.TimerThread;
import com.mpatric.mp3agic.InvalidDataException;
import com.mpatric.mp3agic.UnsupportedTagException;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.dnd.DnDConstants;
import java.awt.dnd.DropTarget;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTextPane;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.JTableHeader;

/**
 * MusicPlayer
 *
 * @author Markus Petritz
 * @version 1.0.0
 */
public class MainViewController extends javax.swing.JFrame {

    private boolean isRunning;
    private boolean isLoop;

    private Integer volume;
    private File lastDirectory;
    private File mp3;
    private JDialog dialog;

    private MusicLibraryModel model;
    private Mp3Metadata metadata;
    private AudioPlayer player;
    private TimerThread timer;

    public MainViewController() {
        initComponents();
        setup();
    }

    private void setup() {
        isRunning = false;
        isLoop = false;
        volume = Integer.parseInt(Configuration.getDefault().get("volume"));
        lastDirectory = new File(Configuration.getDefault().get("lastDirectory"));
        setTooltip();
        generateIcons();
        setDropListener();
        setupTable();
        setupLog();
        initSong();
    }

    private void setTooltip() {
        UIManager.put("ToolTip.foreground", Color.white);
        UIManager.put("ToolTip.background", new Color(45, 45, 45));
        Border border = BorderFactory.createLineBorder(new Color(35, 35, 35), 1);
        UIManager.put("ToolTip.border", border);
        ToolTipManager.sharedInstance().setDismissDelay(15000);
    }

    private void generateIcons() {
        setStopIcon();
        setPlayIcon();
        setLoopIcon();
        setVolumeIcon();
    }

    private void setDropListener() {
        tfFile.setDropTarget(new DropTarget() {
            @Override
            public synchronized void drop(DropTargetDropEvent dtde) {
                try {
                    dtde.acceptDrop(DnDConstants.ACTION_COPY);
                    List<File> droppedFiles = (List<File>) dtde.getTransferable().getTransferData(DataFlavor.javaFileListFlavor);
                    tfFile.setText(droppedFiles.get(0).getAbsolutePath());
                    onAdd(null);
                } catch (UnsupportedFlavorException | IOException ex) {
                    Logger.getDefault().log(ex);
                }
            }
        });

        btOpen.setDropTarget(new DropTarget() {
            @Override
            public synchronized void drop(DropTargetDropEvent dtde) {
                try {
                    dtde.acceptDrop(DnDConstants.ACTION_COPY);
                    List<File> droppedFiles = (List<File>) dtde.getTransferable().getTransferData(DataFlavor.javaFileListFlavor);
                    tfFile.setText(droppedFiles.get(0).getAbsolutePath());
                    onAdd(null);
                } catch (UnsupportedFlavorException | IOException ex) {
                    Logger.getDefault().log(ex);
                }
            }
        });
    }

    private void setupTable() {
        JTableHeader header = tSongs.getTableHeader();
        header.setBackground(new Color(51, 51, 51));
        header.setForeground(Color.white);
        header.setBorder(BorderFactory.createEmptyBorder());

        model = new MusicLibraryModel();
        tSongs.setModel(model);

        tSongs.getColumnModel().getColumn(3).setMaxWidth(100);
        tSongs.getColumnModel().getColumn(4).setMaxWidth(40);
    }

    private void setupLog() {
        JTextPane textPane = new JTextPane();
        textPane.setEditable(false);
        Logger.getDefault().setTextPane(textPane);
        dialog = new JDialog(this, "Log");
        dialog.setContentPane(textPane);
        dialog.setMinimumSize(new Dimension(300, 100));
        dialog.setVisible(false);
    }

    private void setPlayIcon() {
        try {
            BufferedImage icon = null;
            if (!isRunning) {
                icon = ImageIO.read(new File(AssetsHelper.getAssetsPath("ui/icons/play.png")));

            } else {
                icon = ImageIO.read(new File(AssetsHelper.getAssetsPath("ui/icons/pause.png")));
            }
            icon = AssetsHelper.scaleAsset(icon, 45, 45);

            lbIconPlay.setText("");
            lbIconPlay.setIcon(new ImageIcon(AssetsHelper.coloredImage(icon, Color.white)));
            lbIconPlay.addMouseListener(new MouseAdapter() {

                @Override
                public void mouseEntered(MouseEvent e) {
                    lbIconPlay.setIcon(new ImageIcon(AssetsHelper.coloredImage(
                            AssetsHelper.toBufferedImage(lbIconPlay.getIcon()), new Color(191, 191, 191))));
                }

                @Override
                public void mouseClicked(MouseEvent e) {
                    lbIconPlay.setIcon(new ImageIcon(AssetsHelper.coloredImage(
                            AssetsHelper.toBufferedImage(lbIconPlay.getIcon()), new Color(44, 137, 198))));
                }

                @Override
                public void mouseExited(MouseEvent e) {
                    lbIconPlay.setIcon(new ImageIcon(AssetsHelper.coloredImage(
                            AssetsHelper.toBufferedImage(lbIconPlay.getIcon()), Color.white)));
                }

                @Override
                public void mouseReleased(MouseEvent e) {
                    lbIconPlay.setIcon(new ImageIcon(AssetsHelper.coloredImage(
                            AssetsHelper.toBufferedImage(lbIconPlay.getIcon()), Color.white)));
                }
            });
        } catch (IOException e) {
            JOptionPane.showMessageDialog(this, "Unable to retrieve assets pack.");
            System.err.println(e);
        }
    }

    private void setStopIcon() {
        BufferedImage iconStop = AssetsHelper.getAsset(AssetsHelper.getAssetsPath("ui/icons/stop.png"));
        iconStop = AssetsHelper.scaleAsset(iconStop, 40, 40);

        lbIconStop.setText("");
        lbIconStop.setIcon(new ImageIcon(AssetsHelper.coloredImage(iconStop, Color.white)));
        lbIconStop.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseEntered(MouseEvent e) {
                lbIconStop.setIcon(new ImageIcon(AssetsHelper.coloredImage(
                        AssetsHelper.toBufferedImage(lbIconStop.getIcon()), new Color(191, 191, 191))));
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                lbIconStop.setIcon(new ImageIcon(AssetsHelper.coloredImage(
                        AssetsHelper.toBufferedImage(lbIconStop.getIcon()), new Color(44, 137, 198))));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                lbIconStop.setIcon(new ImageIcon(AssetsHelper.coloredImage(
                        AssetsHelper.toBufferedImage(lbIconStop.getIcon()), Color.white)));
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                lbIconStop.setIcon(new ImageIcon(AssetsHelper.coloredImage(
                        AssetsHelper.toBufferedImage(lbIconStop.getIcon()), Color.white)));
            }
        });
    }

    private void setLoopIcon() {
        BufferedImage icon = AssetsHelper.getAsset(AssetsHelper.getAssetsPath("ui/icons/loop.png"));
        icon = AssetsHelper.scaleAsset(icon, 16, 18);

        lbIconLoop.setText("");
        lbIconLoop.setIcon(new ImageIcon(AssetsHelper.coloredImage(icon, Color.white)));
        lbIconLoop.addMouseListener(new MouseAdapter() {

            @Override
            public void mouseEntered(MouseEvent e) {
                if (isLoop) {
                    lbIconLoop.setIcon(new ImageIcon(AssetsHelper.coloredImage(
                            AssetsHelper.toBufferedImage(lbIconLoop.getIcon()), new Color(37, 116, 168))));
                } else {
                    lbIconLoop.setIcon(new ImageIcon(AssetsHelper.coloredImage(
                            AssetsHelper.toBufferedImage(lbIconLoop.getIcon()), new Color(191, 191, 191))));
                }
            }

            @Override
            public void mouseClicked(MouseEvent e) {
                if (isLoop) {
                    lbIconLoop.setIcon(new ImageIcon(AssetsHelper.coloredImage(
                            AssetsHelper.toBufferedImage(lbIconLoop.getIcon()), new Color(37, 116, 168))));
                } else {
                    lbIconLoop.setIcon(new ImageIcon(AssetsHelper.coloredImage(
                            AssetsHelper.toBufferedImage(lbIconLoop.getIcon()), new Color(191, 191, 191))));
                }
            }

            @Override
            public void mouseExited(MouseEvent e) {
                if (isLoop) {
                    lbIconLoop.setIcon(new ImageIcon(AssetsHelper.coloredImage(
                            AssetsHelper.toBufferedImage(lbIconLoop.getIcon()), new Color(44, 137, 198))));
                } else {
                    lbIconLoop.setIcon(new ImageIcon(AssetsHelper.coloredImage(
                            AssetsHelper.toBufferedImage(lbIconLoop.getIcon()), Color.white)));
                }
            }

            @Override
            public void mouseReleased(MouseEvent e) {
                if (isLoop) {
                    lbIconLoop.setIcon(new ImageIcon(AssetsHelper.coloredImage(
                            AssetsHelper.toBufferedImage(lbIconLoop.getIcon()), new Color(44, 137, 198))));
                } else {
                    lbIconLoop.setIcon(new ImageIcon(AssetsHelper.coloredImage(
                            AssetsHelper.toBufferedImage(lbIconLoop.getIcon()), Color.white)));
                }
            }
        });
    }

    private void setVolumeIcon() {
        BufferedImage icon = null;

        if (volume > 0) {
            icon = AssetsHelper.getAsset(AssetsHelper.getAssetsPath("ui/icons/volume.png"));
            icon = AssetsHelper.scaleAsset(icon, 13, 11);
        } else {
            icon = AssetsHelper.getAsset(AssetsHelper.getAssetsPath("ui/icons/volume_off.png"));
            icon = AssetsHelper.scaleAsset(icon, 8, 11);
        }

        lbIconVolume.setText("");
        lbIconVolume.setIcon(new ImageIcon(AssetsHelper.coloredImage(icon, Color.white)));
    }

    private void initSong() {
        lbImageAlbum.setText("");
        BufferedImage album = AssetsHelper.getAsset(AssetsHelper.getAssetsPath("ui/art/album.png"));
        album = AssetsHelper.scaleAsset(album, 60, 60);
        lbImageAlbum.setIcon(new ImageIcon(album));

        lbSongTitle.setText("Title");
        lbSongArtist.setText("Artist");
        lbBeginTime.setText(new Time().toString());
        lbEndTime.setText(new Time().toString());

        sVolume.setValue(volume);
        pnSong.setVisible(false);
    }

    private void updateConfig() {
        Configuration.getDefault().set("volume", volume.toString());
        Configuration.getDefault().set("lastDirectory", lastDirectory.getAbsolutePath());
    }

    private void setSong() {
        try {
            player = new AudioPlayer();
            player.setPath(mp3.getAbsolutePath());

            lbSongTitle.setText(metadata.get().getTitle());
            lbSongArtist.setText(metadata.get().getArtist());
            lbBeginTime.setText(new Time().toString());
            lbEndTime.setText(new Time(metadata.getMp3file().getLengthInSeconds()).toString());

            setTitle(String.format("♫ %s: %s - Music Player", metadata.get().getTitle(), metadata.get().getArtist()));

            int max = (int) metadata.getMp3file().getLengthInSeconds();
            sTime.setMaximum(max);
            sTime.setValue(0);

            //Set Album Cover
            lbImageAlbum.setText("");
            if (metadata.get().getAlbumImage() != null && !metadata.get().getAlbumImageMimeType().isEmpty()) {
                InputStream in = new ByteArrayInputStream(metadata.get().getAlbumImage());
                BufferedImage bImageFromConvert = ImageIO.read(in);
                bImageFromConvert = AssetsHelper.scaleAsset(bImageFromConvert, 60, 60);
                lbImageAlbum.setIcon(new ImageIcon(bImageFromConvert));
            } else {
                BufferedImage album = AssetsHelper.getAsset(AssetsHelper.getAssetsPath("ui/art/album.png"));
                album = AssetsHelper.scaleAsset(album, 60, 60);
                lbImageAlbum.setIcon(new ImageIcon(album));
            }
            if (metadata.get().getAlbum() == null || metadata.get().getAlbum().isEmpty()) {
                lbImageAlbum.setToolTipText("Album");
            } else {
                lbImageAlbum.setToolTipText(metadata.get().getAlbum());
            }

        } catch (NullPointerException ex) {
            Logger.getDefault().log(ex);
        } catch (Exception ex) {
            if (ex.getMessage() == null) {
                JOptionPane.showMessageDialog(this, "Unknown Error. Please inform your administrator.");
            } else {
                JOptionPane.showMessageDialog(this, ex.getMessage());
            }
            Logger.getDefault().log(ex);
        } finally {
            pnSong.setVisible(true);
        }
    }

    public void updateTime() {
        if (player != null && timer != null) {
            sTime.setValue((int) timer.getCurrentTime());
            lbBeginTime.setText(new Time(timer.getCurrentTime()).toString());
        } else {
            lbBeginTime.setText(new Time().toString());
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        pnMainContent = new javax.swing.JPanel();
        pnSong = new javax.swing.JPanel();
        lbSongTitle = new javax.swing.JLabel();
        lbSongArtist = new javax.swing.JLabel();
        lbImageAlbum = new javax.swing.JLabel();
        pnMain = new javax.swing.JPanel();
        pnAdd = new javax.swing.JPanel();
        tfFile = new javax.swing.JTextField();
        btOpen = new javax.swing.JButton();
        pnList = new javax.swing.JPanel();
        spList = new javax.swing.JScrollPane();
        tSongs = new javax.swing.JTable();
        pnControlContent = new javax.swing.JPanel();
        lbIconPlay = new javax.swing.JLabel();
        lbIconStop = new javax.swing.JLabel();
        lbBeginTime = new javax.swing.JLabel();
        sTime = new javax.swing.JSlider();
        lbEndTime = new javax.swing.JLabel();
        lbIconLoop = new javax.swing.JLabel();
        lbIconVolume = new javax.swing.JLabel();
        sVolume = new javax.swing.JSlider();
        pnTitleContent = new javax.swing.JPanel();
        lbTitle = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Music Player");
        setMinimumSize(new java.awt.Dimension(616, 319));
        setSize(new java.awt.Dimension(823, 369));

        pnMainContent.setBackground(new java.awt.Color(51, 51, 51));
        pnMainContent.setLayout(new java.awt.BorderLayout());

        pnSong.setBackground(new java.awt.Color(33, 33, 33));
        pnSong.setLayout(new java.awt.GridBagLayout());

        lbSongTitle.setFont(new java.awt.Font("Open Sans", 1, 16)); // NOI18N
        lbSongTitle.setForeground(new java.awt.Color(255, 255, 255));
        lbSongTitle.setText("Title");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(10, 4, 4, 4);
        pnSong.add(lbSongTitle, gridBagConstraints);

        lbSongArtist.setFont(new java.awt.Font("Open Sans", 0, 14)); // NOI18N
        lbSongArtist.setForeground(new java.awt.Color(255, 255, 255));
        lbSongArtist.setText("Artist");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(4, 4, 14, 4);
        pnSong.add(lbSongArtist, gridBagConstraints);

        lbImageAlbum.setForeground(new java.awt.Color(255, 255, 255));
        lbImageAlbum.setText("Album Cover");
        lbImageAlbum.setToolTipText("Album Name");
        lbImageAlbum.setPreferredSize(new java.awt.Dimension(60, 60));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(8, 8, 8, 8);
        pnSong.add(lbImageAlbum, gridBagConstraints);

        pnMainContent.add(pnSong, java.awt.BorderLayout.PAGE_END);

        pnMain.setBackground(new java.awt.Color(51, 51, 51));
        pnMain.setLayout(new java.awt.BorderLayout());

        pnAdd.setBackground(new java.awt.Color(51, 51, 51));
        pnAdd.setLayout(new java.awt.GridBagLayout());

        tfFile.setBackground(new java.awt.Color(68, 68, 68));
        tfFile.setColumns(20);
        tfFile.setFont(new java.awt.Font("Open Sans", 0, 14)); // NOI18N
        tfFile.setForeground(new java.awt.Color(255, 255, 255));
        tfFile.setText("Click to choose File");
        tfFile.setSelectedTextColor(new java.awt.Color(255, 255, 255));
        tfFile.setSelectionColor(new java.awt.Color(44, 137, 198));
        tfFile.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                onFileChoose(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.ipadx = 8;
        gridBagConstraints.ipady = 8;
        pnAdd.add(tfFile, gridBagConstraints);

        btOpen.setForeground(new java.awt.Color(44, 137, 198));
        btOpen.setIcon(new javax.swing.ImageIcon(getClass().getResource("/at/petritzdesigns/musicplayer/assets/ui/art/add_button.png"))); // NOI18N
        btOpen.setBorderPainted(false);
        btOpen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                onAdd(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(3, 0, 3, 4);
        pnAdd.add(btOpen, gridBagConstraints);

        pnMain.add(pnAdd, java.awt.BorderLayout.PAGE_START);

        pnList.setBackground(new java.awt.Color(51, 51, 51));
        pnList.setLayout(new java.awt.BorderLayout());

        spList.setBackground(new java.awt.Color(51, 51, 51));

        tSongs.setAutoCreateRowSorter(true);
        tSongs.setBackground(new java.awt.Color(51, 51, 51));
        tSongs.setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        tSongs.setForeground(new java.awt.Color(255, 255, 255));
        tSongs.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tSongs.setFillsViewportHeight(true);
        tSongs.setIntercellSpacing(new java.awt.Dimension(2, 2));
        tSongs.setRowHeight(40);
        tSongs.setSelectionBackground(new java.awt.Color(37, 116, 168));
        tSongs.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        tSongs.setShowGrid(true);
        tSongs.setShowVerticalLines(false);
        tSongs.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                onPlaySong(evt);
            }
        });
        spList.setViewportView(tSongs);

        pnList.add(spList, java.awt.BorderLayout.CENTER);

        pnMain.add(pnList, java.awt.BorderLayout.CENTER);

        pnMainContent.add(pnMain, java.awt.BorderLayout.CENTER);

        getContentPane().add(pnMainContent, java.awt.BorderLayout.CENTER);

        pnControlContent.setBackground(new java.awt.Color(38, 38, 38));
        pnControlContent.setLayout(new java.awt.GridBagLayout());

        lbIconPlay.setForeground(new java.awt.Color(255, 255, 255));
        lbIconPlay.setText("Play Button");
        lbIconPlay.setToolTipText("Play");
        lbIconPlay.setPreferredSize(new java.awt.Dimension(50, 50));
        lbIconPlay.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                onPlayClicked(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(7, 7, 7, 7);
        pnControlContent.add(lbIconPlay, gridBagConstraints);

        lbIconStop.setForeground(new java.awt.Color(255, 255, 255));
        lbIconStop.setText("Stop Button");
        lbIconStop.setToolTipText("Stop");
        lbIconStop.setPreferredSize(new java.awt.Dimension(50, 50));
        lbIconStop.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                onStopClicked(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(7, 7, 7, 7);
        pnControlContent.add(lbIconStop, gridBagConstraints);

        lbBeginTime.setForeground(new java.awt.Color(255, 255, 255));
        lbBeginTime.setText("00:00");
        lbBeginTime.setToolTipText("");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_END;
        gridBagConstraints.insets = new java.awt.Insets(2, 10, 2, 2);
        pnControlContent.add(lbBeginTime, gridBagConstraints);

        sTime.setBackground(new java.awt.Color(38, 38, 38));
        sTime.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                onTimeChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.BOTH;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 2);
        pnControlContent.add(sTime, gridBagConstraints);

        lbEndTime.setForeground(new java.awt.Color(255, 255, 255));
        lbEndTime.setText("00:00");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.LINE_START;
        gridBagConstraints.insets = new java.awt.Insets(2, 2, 2, 10);
        pnControlContent.add(lbEndTime, gridBagConstraints);

        lbIconLoop.setForeground(new java.awt.Color(255, 255, 255));
        lbIconLoop.setText("Loop Button");
        lbIconLoop.setToolTipText("Wiederholen");
        lbIconLoop.setPreferredSize(new java.awt.Dimension(20, 20));
        lbIconLoop.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                onLoop(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(0, 10, 0, 5);
        pnControlContent.add(lbIconLoop, gridBagConstraints);

        lbIconVolume.setForeground(new java.awt.Color(255, 255, 255));
        lbIconVolume.setText("Volume Icon");
        lbIconVolume.setToolTipText("Lautstärke");
        lbIconVolume.setPreferredSize(new java.awt.Dimension(20, 20));
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new java.awt.Insets(0, 15, 0, 0);
        pnControlContent.add(lbIconVolume, gridBagConstraints);

        sVolume.setBackground(new java.awt.Color(38, 38, 38));
        sVolume.setPreferredSize(new java.awt.Dimension(100, 29));
        sVolume.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                onVolumeChanged(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 1;
        pnControlContent.add(sVolume, gridBagConstraints);

        getContentPane().add(pnControlContent, java.awt.BorderLayout.PAGE_END);

        pnTitleContent.setBackground(new java.awt.Color(38, 38, 38));
        pnTitleContent.setLayout(new java.awt.GridBagLayout());

        lbTitle.setFont(new java.awt.Font("Open Sans", 0, 36)); // NOI18N
        lbTitle.setForeground(new java.awt.Color(255, 255, 255));
        lbTitle.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbTitle.setText("Music Player");
        lbTitle.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                onEasterEgg(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.NORTHWEST;
        gridBagConstraints.insets = new java.awt.Insets(25, 0, 25, 0);
        pnTitleContent.add(lbTitle, gridBagConstraints);

        getContentPane().add(pnTitleContent, java.awt.BorderLayout.PAGE_START);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void onPlayClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_onPlayClicked
        if (player != null) {
            if (isRunning) {
                player.pause();
                timer.suspend();
            } else {
                timer = new TimerThread(this, player, metadata.getMp3file().getLengthInSeconds());
                if (player.canResume()) {
                    player.resume();
                    timer.resume();
                } else {
                    if (player.isRunning()) {
                        isRunning = true;
                        onPlayClicked(evt);
                    } else {
                        player.play(0);
                        timer.start();
                    }
                }
            }

            isRunning = !isRunning;
            setPlayIcon();
        }
    }//GEN-LAST:event_onPlayClicked

    private void onStopClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_onStopClicked
        if (player != null) {
            isRunning = false;
            setPlayIcon();
            player.stop();
            timer.interrupt();
            timer.stop();
            timer = null;
            setSong();
        }
    }//GEN-LAST:event_onStopClicked

    private void onFileChoose(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_onFileChoose
        FileNameExtensionFilter filter = new FileNameExtensionFilter("MP3 Song", "mp3");
        JFileChooser chooser = new JFileChooser();
        chooser.setAcceptAllFileFilterUsed(false);
        chooser.setFileFilter(filter);
        chooser.setCurrentDirectory(lastDirectory);
        int val = chooser.showOpenDialog(this);
        if (val == JFileChooser.APPROVE_OPTION) {
            File file = chooser.getSelectedFile();
            lastDirectory = file;
            tfFile.setText(file.getAbsolutePath());
            onAdd(null);
        }
    }//GEN-LAST:event_onFileChoose

    private void onAdd(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_onAdd
        try {
            if (tfFile.getText().trim().isEmpty()) {
                onFileChoose(null);
            }
            if (!tfFile.getText().endsWith(".mp3")) {
                onFileChoose(null);
            }
            if (!AssetsHelper.isValidMp3File(new File(tfFile.getText()))) {
                throw new Exception("File is not valid!");
            }
            mp3 = new File(tfFile.getText());
            metadata = new Mp3Metadata(mp3);
            updateConfig();
            model.addMusic(new Music(metadata, mp3));
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage());
        }
    }//GEN-LAST:event_onAdd

    private void onLoop(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_onLoop
        isLoop = !isLoop;
        player.setLoop(isLoop);
    }//GEN-LAST:event_onLoop

    private void onVolumeChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_onVolumeChanged
        volume = sVolume.getValue();
        setVolumeIcon();
        updateConfig();
    }//GEN-LAST:event_onVolumeChanged

    private void onTimeChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_onTimeChanged
        if (sTime.getValue() != timer.getCurrentTime()) {
            player.pause();
            player.setStopped((int) (sTime.getValue() * (player.getTotalBytes() / metadata.getMp3file().getLengthInSeconds())));
            timer.suspend();
            timer.setCurrentTime(sTime.getValue());
            player.resume();
            timer.resume();
        }
    }//GEN-LAST:event_onTimeChanged

    private void onPlaySong(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_onPlaySong
        //Play selected Song
        if (evt.getClickCount() == 2) {
            try {
                int index = tSongs.getSelectedRow();
                if (index < 0) {
                    throw new Exception("No selected row");
                }

                //stop selected song
                onStopClicked(evt);

                mp3 = model.getLibrary().get(index).getFile();
                metadata = new Mp3Metadata(mp3);
                setSong();
                onPlayClicked(evt);
            } catch (IOException | UnsupportedTagException | InvalidDataException ex) {
                JOptionPane.showMessageDialog(this, ex.getMessage());
                Logger.getDefault().log(ex);
            } catch (Exception ex) {
                Logger.getDefault().log(ex);
            }
        }
    }//GEN-LAST:event_onPlaySong

    private void onEasterEgg(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_onEasterEgg
        if (evt.getClickCount() == 3) {
            if(dialog.isVisible()) {
                dialog.setVisible(false);
            }
            else {
                dialog.setVisible(true);
            }
        }
    }//GEN-LAST:event_onEasterEgg

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btOpen;
    private javax.swing.JLabel lbBeginTime;
    private javax.swing.JLabel lbEndTime;
    private javax.swing.JLabel lbIconLoop;
    private javax.swing.JLabel lbIconPlay;
    private javax.swing.JLabel lbIconStop;
    private javax.swing.JLabel lbIconVolume;
    private javax.swing.JLabel lbImageAlbum;
    private javax.swing.JLabel lbSongArtist;
    private javax.swing.JLabel lbSongTitle;
    private javax.swing.JLabel lbTitle;
    private javax.swing.JPanel pnAdd;
    private javax.swing.JPanel pnControlContent;
    private javax.swing.JPanel pnList;
    private javax.swing.JPanel pnMain;
    private javax.swing.JPanel pnMainContent;
    private javax.swing.JPanel pnSong;
    private javax.swing.JPanel pnTitleContent;
    private javax.swing.JSlider sTime;
    private javax.swing.JSlider sVolume;
    private javax.swing.JScrollPane spList;
    private javax.swing.JTable tSongs;
    private javax.swing.JTextField tfFile;
    // End of variables declaration//GEN-END:variables

}
